// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyProject45_gitGameMode.h"
#include "MyProject45_gitHUD.h"
#include "MyProject45_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyProject45_gitGameMode::AMyProject45_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyProject45_gitHUD::StaticClass();
}
