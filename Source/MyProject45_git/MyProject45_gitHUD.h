// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyProject45_gitHUD.generated.h"

UCLASS()
class AMyProject45_gitHUD : public AHUD
{
	GENERATED_BODY()

public:
	AMyProject45_gitHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

