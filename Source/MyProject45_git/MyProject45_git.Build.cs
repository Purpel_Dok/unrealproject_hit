// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MyProject45_git : ModuleRules
{
	public MyProject45_git(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
