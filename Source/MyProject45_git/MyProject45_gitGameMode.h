// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyProject45_gitGameMode.generated.h"

UCLASS(minimalapi)
class AMyProject45_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyProject45_gitGameMode();
};



